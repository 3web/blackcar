import React from "react"
import HeaderStyle from "./HeaderStyle.module.styl"
import Logo from "./../../assets/images/logo.svg"
import Phone from "./../../assets/images/noun_Phone_2341792.svg"
import Facebook from "./../../assets/images/facebook-letter-logo.svg"
import Instagram from "./../../assets/images/instagram-2.svg"
import Yelp from "./../../assets/images/yelp.svg"
import { Link } from "gatsby"

const Header = () => (
  <header className={HeaderStyle.header}>
    <div className={HeaderStyle.header_content}>
      <Link to="/">
        <img className={HeaderStyle.logo} src={Logo} alt={"logo"} />
      </Link>
      <div className={HeaderStyle.info_section}>
        <a href="tel:9497639491" className={HeaderStyle.phone_section}>
          <img src={Phone} alt={"phone"} /> 9497639491
        </a>
        <div className={HeaderStyle.socials_section}>
          <a
            href="https://www.facebook.com/blackcarnewport"
            target="_blank"
            rel="noreferrer"
          >
            <img src={Facebook} alt="facebook" />
          </a>

          <a
            href="https://www.instagram.com/blackcarnewport/"
            target="_blank"
            rel="noreferrer"
          >
            <img src={Instagram} alt="instagram" />
          </a>
          <a
            href="https://www.yelp.ie/biz/black-car-newport-irvine-3"
            target="_blank"
            rel="noreferrer"
          >
            <img src={Yelp} alt="yelp" />
          </a>
        </div>
      </div>
    </div>
  </header>
)

export default Header
