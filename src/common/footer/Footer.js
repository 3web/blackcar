import React from "react"
import FooterStyle from "./FooterStyle.module.styl"
import HeaderStyle from "../header/HeaderStyle.module.styl"
import Facebook from "../../assets/images/facebook-letter-logo.svg"
import Instagram from "../../assets/images/instagram-2.svg"
import Yelp from "../../assets/images/yelp.svg"

const Footer = props => {
  return (
    <footer className={FooterStyle.footer}>
      <div className={HeaderStyle.socials_section}>
        <a
          href="https://www.facebook.com/blackcarnewport"
          target="_blank"
          rel="noreferrer"
        >
          <img src={Facebook} alt="facebook" />
        </a>

        <a
          href="https://www.instagram.com/blackcarnewport/"
          target="_blank"
          rel="noreferrer"
        >
          <img src={Instagram} alt="instagram" />
        </a>
        <a
          href="https://www.yelp.ie/biz/black-car-newport-irvine-3"
          target="_blank"
          rel="noreferrer"
        >
          <img src={Yelp} alt="yelp" />
        </a>
      </div>
    </footer>
  )
}

export default Footer
