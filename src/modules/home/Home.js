import React from "react"
import HomeStyle from "./HomeStyle.module.styl"

const Home = props => {
  return (
    <div className={HomeStyle.main_page}>
      <div className={HomeStyle.main_page_content}>
        <h1>YOUR PRIVATE DRIVER</h1>
        <div className={HomeStyle.contacts_wrapper}>
          <a href="tel:9497639491" className={HomeStyle.button}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="29.805"
              height="29.802"
              viewBox="0 0 29.805 29.802"
            >
              <g transform="translate(-8.75 -8.758)">
                <g transform="translate(8.75 8.758)">
                  <path
                    className="a"
                    d="M33.5,27.081a2.043,2.043,0,0,0-3.106.144q-1.137,1.029-2.167,2.167a.968.968,0,0,1-1.408.253A23.849,23.849,0,0,1,17.615,20.4a.968.968,0,0,1,.217-1.336c.758-.722,1.517-1.481,2.2-2.239a2.193,2.193,0,0,0,.072-3.142c-1.336-1.408-2.744-2.817-4.153-4.153a2.119,2.119,0,0,0-3.178-.108c-1.444,1.336-3.069,2.564-3.864,4.478a6.341,6.341,0,0,0,0,2.853,28.767,28.767,0,0,0,5.561,10.8A30.653,30.653,0,0,0,29.387,37.987c.433.144.9.253,1.336.4a4.99,4.99,0,0,0,2.636,0c1.914-.794,3.142-2.419,4.478-3.864a2.078,2.078,0,0,0,0-3.142C36.465,29.9,35.02,28.453,33.5,27.081Z"
                    transform="translate(-8.75 -8.758)"
                  />
                </g>
              </g>
            </svg>
            9497639491
          </a>
          <a
            href="mailto:blackcarnewport@gmail.com"
            className={HomeStyle.email_button}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="33.178"
              height="23.347"
              viewBox="0 0 33.178 23.347"
            >
              <g transform="translate(-9 -23)">
                <g transform="translate(9 23)">
                  <path
                    className="a"
                    d="M11.428,975.362a2.3,2.3,0,0,0-.91.189l14.111,13.241a1.248,1.248,0,0,0,1.9-.013l14.124-13.227a2.292,2.292,0,0,0-.9-.189H11.428Zm-2.4,2.229a2.865,2.865,0,0,0-.025.365v18.159a2.51,2.51,0,0,0,2.428,2.594H39.751a2.51,2.51,0,0,0,2.428-2.594V977.956a2.865,2.865,0,0,0-.025-.365L28.131,990.738a3.706,3.706,0,0,1-5.108,0Z"
                    transform="translate(-9 -975.362)"
                  />
                </g>
              </g>
            </svg>
            blackcarnewport@gmail.com
          </a>
          <a
            href="mailto:blackcarnewport@gmail.com"
            className={HomeStyle.mobile_button}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="33.178"
              height="23.347"
              viewBox="0 0 33.178 23.347"
            >
              <g transform="translate(-9 -23)">
                <g transform="translate(9 23)">
                  <path
                    className="a"
                    d="M11.428,975.362a2.3,2.3,0,0,0-.91.189l14.111,13.241a1.248,1.248,0,0,0,1.9-.013l14.124-13.227a2.292,2.292,0,0,0-.9-.189H11.428Zm-2.4,2.229a2.865,2.865,0,0,0-.025.365v18.159a2.51,2.51,0,0,0,2.428,2.594H39.751a2.51,2.51,0,0,0,2.428-2.594V977.956a2.865,2.865,0,0,0-.025-.365L28.131,990.738a3.706,3.706,0,0,1-5.108,0Z"
                    transform="translate(-9 -975.362)"
                  />
                </g>
              </g>
            </svg>
            Email Us
          </a>
        </div>
      </div>
    </div>
  )
}

export default Home
