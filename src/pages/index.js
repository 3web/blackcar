import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import Home from "../modules/home/Home"

const IndexPage = () => (
  <Layout>
    <SEO title="Black Car Newport" />
    <Home />
  </Layout>
)

export default IndexPage
